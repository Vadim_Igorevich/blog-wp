<?php
namespace laravelwp;

/**
 * Curl based laravelwp client.
 *
 * The request HTTP status code always suppressed to 200. Real HTTP status you can see in response body.
 * Response body represents:
 * - success: bool - is request handled properly.
 * - status: int - HTTP status code
 * - data: - requested data.
 */
class Client
{
    const API_URL = 'http://127.0.0.1:8000/api/';

    public $apiUri = self::API_URL;

    public $language = 'ru';

    public function __construct()
    {
        $this->apiUri = self::API_URL;
//        $this->apiUri = get_option('link_to_crm_api').'/';
    }

    public function getPosts($data = [])
    {
        return $this->request($this->apiUri . 'posts', 'GET', $data);
    }

    public function getPost($slug)
    {
        return $this->request($this->apiUri . 'posts/' . $slug, 'GET', []);
    }

    public function request($uri, $method = 'GET', array $data = [], $accessToken = null)
    {
        $data = http_build_query($data, '', '&');

        $headers = array(
            'Cache-Control: no-cache',
        );

        $curlOptions = [
            \CURLOPT_VERBOSE => false,
            \CURLOPT_SSL_VERIFYHOST => false,
            \CURLOPT_SSL_VERIFYPEER => false,
        ];

        if ($accessToken) {
            $curlOptions[\CURLOPT_HTTPAUTH] = \CURLAUTH_BASIC;
            $curlOptions[\CURLOPT_USERPWD] = $accessToken . ":";
        }

        if ('GET' === $method) {
            $curlOptions[\CURLOPT_HTTPGET] = true;
            $curlOptions[\CURLOPT_URL] = $uri . '?' . $data;
        } elseif ('POST' === $method) {
            $curlOptions[\CURLOPT_POST] = true;
            $curlOptions[\CURLOPT_URL] = $uri;
            $curlOptions[\CURLOPT_POSTFIELDS] = $data;
        } else {
            throw new \DomainException(sprintf('An HTTP method "%s" is not supported. Use "GET" or "POST".', $method));
        }

        $curlOptions[\CURLOPT_HTTPHEADER] = $headers;
        $curlOptions[\CURLOPT_RETURNTRANSFER] = true;

        $curl = curl_init();
        curl_setopt_array($curl, $curlOptions);

        $response = curl_exec($curl);

        if (false === $response) {
            $error = curl_error($curl);
            $errorCode = curl_errno($curl);

            throw new NetworkException('Curl error: ' . $error, $errorCode);
        }

        $response = json_decode($response);

        return $response;
    }
}
