<?php
namespace laravelwp;

class Model
{
    protected $errors = [];
    protected $values = [];

    public function load($data)
    {
        foreach ($data as $attribute => $value) {
            $this->setValue($attribute, $value);
        }
    }

    public function setError($attribute, $message)
    {
        $this->errors[$attribute] = $message;
    }

    public function getError($attribute)
    {
        return isset($this->errors[$attribute]) ? $this->errors[$attribute] : null;
    }

    public function hasError($attribute)
    {
        return isset($this->errors[$attribute]) && $this->errors[$attribute];
    }

    public function setValue($attribute, $value)
    {
        $this->values[$attribute] = $value;
    }

    public function getValue($attribute, $defaultValue = null)
    {
        return isset($this->values[$attribute]) ? $this->values[$attribute] : $defaultValue;
    }
}