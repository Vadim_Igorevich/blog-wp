<?php

namespace laravelwp;

class Pagination {
    public $total = 0;
    public $number = 1;
    public $limit = 20;
    public $num_links = 8;
    public $url = '';
    public $text_first = '|&lt;';
    public $text_last = '&gt;|';
    public $text_next = '&gt;';
    public $text_prev = '&lt;';

    public function render() {
        $total = $this->total;

        if ($this->number < 1) {
            $number = 1;
        } else {
            $number = $this->number;
        }

        if (!(int)$this->limit) {
            $limit = 10;
        } else {
            $limit = $this->limit;
        }

        $num_links = $this->num_links;
        $num_numbers = ceil($total / $limit);

        $this->url = str_replace('%7Bnumber%7D', '{number}', $this->url);

        $output = '<nav class="pagination-block"><ul class="pagination">';

        if ($number > 1) {
            //  $output .= '<li class="number-item"><a href="' . str_replace(array('&amp;number={number}', '?number={number}', '&number={number}'), '', $this->url) . '">' . $this->text_first . '</a></li>';

            if ($number - 1 === 1) {
                $output .= '<li><a href="' . str_replace(array('&amp;number={number}', '?number={number}', '&number={number}'), '', $this->url) . '"><span aria-hidden="true">«</span></a></li>';
            } else {
                $output .= '<li><a href="' . str_replace('{number}', $number - 1, $this->url) . '">' . $this->text_prev . '</a></li>';
            }
        }

        if ($num_numbers > 1) {
            if ($num_numbers <= $num_links) {
                $start = 1;
                $end = $num_numbers;
            } else {
                $start = $number - floor($num_links / 2);
                $end = $number + floor($num_links / 2);

                if ($start < 1) {
                    $end += abs($start) + 1;
                    $start = 1;
                }

                if ($end > $num_numbers) {
                    $start -= ($end - $num_numbers);
                    $end = $num_numbers;
                }
            }

            for ($i = $start; $i <= $end; $i++) {
                if ($number == $i) {
                    $output .= '<li class="active"><span>' . $i . '</span></li>';
                } else {
                    if ($i === 1) {
                        $output .= '<li><a href="' . str_replace(array('&amp;number={number}', '?number={number}', '&number={number}'), '', $this->url) . '">' . $i . '</a></li>';
                    } else {
                        $output .= '<li><a href="' . str_replace('{number}', $i, $this->url) . '">' . $i . '</a></li>';
                    }
                }
            }
        }

        if ($number < $num_numbers) {
            $output .= '<li><a href="' . str_replace('{number}', $number + 1, $this->url) . '"><span>»</span></a></li>';
            //$output .= '<li><a href="' . str_replace('{number}', $num_numbers, $this->url) . '">' . $this->text_last . '</a></li>';
        }

        $output .= '</ul>';

        if ($num_numbers > 1) {
            return $output;
        } else {
            return '';
        }
    }
}