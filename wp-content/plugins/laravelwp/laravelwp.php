<?php

/*ini_set('display_errors', 1);
error_reporting(E_ALL);*/
/**
 * Plugin Name: Laravel Client
 */
if (!session_id()) {
    session_start();
}
// namespace autoloader
function laravelwp_autoloader($class)
{
    // echo $class;
    if (strpos($class, "laravelwp\\") !== 0) {
        return;
    }

    $class = str_replace("laravelwp\\", '', $class);

    $filename = __DIR__ . '/' . str_replace('\\', '/', $class) . '.php';
    include($filename);
}
spl_autoload_register('laravelwp_autoloader');

add_action( 'plugins_loaded', array( 'laravelwp\Plugin', 'getInstance' ) );
