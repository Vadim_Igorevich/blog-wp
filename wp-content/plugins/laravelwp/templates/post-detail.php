<?php

$slug = $_GET['slug'];

if (!$slug) {
    wp_redirect ( home_url() );
}

$plugin = \laravelwp\Plugin::getInstance();
$postData = $plugin->client->getPost($slug);

if (!$postData) {
    global $wp_query;
    $wp_query->set_404();
    status_header(404);
    get_template_part(404);
    exit();
}

$post = $postData->data;

?>

<?php get_header(); get_the_title();?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/gridlex/2.7.1/gridlex.min.css">
<link type="text/css" media="all" href="<?=home_url()?>/wp-content/plugins/laravelwp/assets/bootstrap/css/bootstrap.css" rel="stylesheet" />
<link type="text/css" media="all" href="<?=home_url()?>/wp-content/plugins/laravelwp/assets/bootstrap/css/bootstrap-theme.css" rel="stylesheet" />
<link type="text/css" media="all" href="<?=home_url()?>/wp-content/plugins/laravelwp/assets/css/custom.css" rel="stylesheet" />

<div class="container bg-grey" style="min-height: 500px;">
    <h1 class="text-left font-weight-bold font-size-xl"><?= $post->title ?></h1>

    <div class="grid">
        <div class="col-4_sm-12">
            <?php if ($post->image): ?>
                <img style="max-width: 100%;" src="<?=$post->image->links->display?>" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" loading="lazy" srcset="<?=$post->image->links->display?> 332w, <?=$post->image->links->display?> 300w" sizes="(max-width: 332px) 100vw, 332px">
            <?php else: ?>
                <img style="max-width: 100%;" src="https://www.ovationthemes.com/demos/ovation-blog-pro/wp-content/uploads/sites/6/2021/04/feature6.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" loading="lazy" srcset="https://www.ovationthemes.com/demos/ovation-blog-pro/wp-content/uploads/sites/6/2021/04/feature6.jpg 332w, https://www.ovationthemes.com/demos/ovation-blog-pro/wp-content/uploads/sites/6/2021/04/feature6-300x257.jpg 300w" sizes="(max-width: 332px) 100vw, 332px">
            <?php endif; ?>

            <div class="postbox-content">
                <div class="meta_post">
                    <div class="date-box"><span class="date-box-inner"><?= date('d.m.Y', strtotime($post->updated_at)) ?></span></div>
                </div>
                <h4 class="posttitle mb-0"><a href="<?=home_url()?>/post?slug=<?= $post->slug ?>"><?= $post->title ?></a></h4>
                <span class="mr-3"><i class="fas fa-user"></i> <?= $post->author->name ?></span>
            </div>
        </div>
        <div class="col-8_sm-12">
            <div class="mb-3 post-text"><?= $post->content ?></div>
        </div>
    </div>
</div>


<?php get_footer(); ?>


