<?php

$page = $_GET['number'];

$plugin = \laravelwp\Plugin::getInstance();
$result = $plugin->client->getPosts([
    'page' => $page
]);

$posts = $result->data;
$links = $result->links;
$meta = $result->meta;
$currentPage = $meta->current_page;
$lastPage = $meta->last_page;
$perPage = $meta->per_page;
$totalPages = $meta->total;
?>

<?php get_header(); get_the_title();?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/gridlex/2.7.1/gridlex.min.css">
<link type="text/css" media="all" href="<?=home_url()?>/wp-content/plugins/laravelwp/assets/bootstrap/css/bootstrap.css" rel="stylesheet" />
<link type="text/css" media="all" href="<?=home_url()?>/wp-content/plugins/laravelwp/assets/bootstrap/css/bootstrap-theme.css" rel="stylesheet" />
<link type="text/css" media="all" href="<?=home_url()?>/wp-content/plugins/laravelwp/assets/css/custom.css" rel="stylesheet" />


<div class="container bg-grey">
    <h1 class="text-center font-weight-bold font-size-xl"><?= get_the_title() ?></h1>
    <?php
    if (!empty($posts)) { ?>
        <div class="grid-3_xs-1">
                    <?php
                foreach ($posts as $key => $post) { ?>
<!--                    echo '<div class="col">'.$key.'<a href="http://localhost/wordpress/post?slug='.$post->slug.'">'.$post->title.'</a></div>';-->
                    <div id="single_post" class="col-lg-6 col-md-6 col-sm-12 mt-3 mb-3">
                        <div class="post-slide">
                            <div class="post_pic_inner">
                                <a href="<?=home_url()?>/post?slug=<?= $post->slug ?>" class="over-layer">
                                    <?php if ($post->image): ?>
                                        <img width="332" height="284" src="<?=$post->image->links->display?>" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" loading="lazy" srcset="<?=$post->image->links->display?> 332w, <?=$post->image->links->display?> 300w" sizes="(max-width: 332px) 100vw, 332px">
                                    <?php else: ?>
                                        <img width="332" height="284" src="https://www.ovationthemes.com/demos/ovation-blog-pro/wp-content/uploads/sites/6/2021/04/feature6.jpg" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" loading="lazy" srcset="https://www.ovationthemes.com/demos/ovation-blog-pro/wp-content/uploads/sites/6/2021/04/feature6.jpg 332w, https://www.ovationthemes.com/demos/ovation-blog-pro/wp-content/uploads/sites/6/2021/04/feature6-300x257.jpg 300w" sizes="(max-width: 332px) 100vw, 332px">
                                    <?php endif; ?>
                                </a>
                            </div>
                            <div class="postbox-content">
                                <div class="meta_post">
                                    <div class="date-box"><span class="date-box-inner"><?= date('d.m.Y', strtotime($post->updated_at)) ?></span></div>
                                </div>
                                <h4 class="posttitle mb-0"><a href="<?=home_url()?>/post?slug=<?= $post->slug ?>"><?= $post->title ?></a></h4>
                                <div class="mb-3 post-text"><?= $post->description ?></div>
                                <span class="mr-3"><i class="fas fa-user"></i> <?= $post->author->name ?></span>
                            </div>
                        </div>
                    </div>
                <?php }

                $pagination = new \laravelwp\Pagination\data\Pagination([
                    'totalCount' => $totalPages,
                ]);

                ?>
        </div>


        <div>
            <?=\laravelwp\Pagination\widgets\Pagination::widget([
                'pagination' => $pagination
            ])?>
        </div>
        <?php
    }

    ?>
</div>


<?php get_footer(); ?>

